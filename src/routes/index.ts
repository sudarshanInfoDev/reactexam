import { lazy } from 'react';
import { externalRoute } from './DistinctRoute/External';
import { internalRoute } from './DistinctRoute/Internal';

const HomePage = lazy(() => import("../core/Public/Homepage"));
const CartPage = lazy(() => import("../core/Public/CartPage"));
const Register = lazy(() => import("../core/Public/Register/Register"));

const Boundary = lazy(() => import("../core/Protected/Boundary"));


export const appRoutes: CustomRoute[] = [
    
    {
        path: "/cart",
        component: CartPage,
        type: "unauthorized"
    },
    {
        path: "/",
        component: HomePage,
        type: "unauthorized"
    },
    {
        path: "/register",
        component: Register,
        type: "unauthorized"
    },
    {
        path: "/",
        component: Boundary,
        children: [...internalRoute, ...externalRoute],
    }
]
