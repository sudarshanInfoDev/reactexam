import Product from "components/UI/Product.js";
import React, { ReactElement, useEffect } from "react";
import { connect, ConnectedProps } from "react-redux";
import { AddToCart, RemoveFromCart } from "store/modules/Cart/actions";
import { getProducts } from "store/modules/Product/productReducer";
import { RootState } from "store/root-reducer";

//from boilerplate
export interface UserCredentials {
  username: string;
  password: string;
}

interface Props extends PropsFromRedux {}

function HomePage(props: Props): ReactElement {
  const {
    products,
    cartItems,
    ActionAddToCart,
    ActionRemoveFromCart,
    productLoading,
  } = props;
  useEffect(() => {
    props.getProducts();
  }, []);

  const handleProductsCart = (productId, type) => {
    const productSelected = products?.find((x: any) => x?._id === productId);

    console.log(productId, type);

    if (type === "add") {
      ActionAddToCart(productSelected);
    } else if (type === "remove") {
      ActionRemoveFromCart(productSelected);
    }
  };

  const getCartQuantityNumber = (id) => {
    const isInCart = cartItems.find((x) => x?._id === id);
    if (isInCart) {
      return cartItems.find((x) => x?._id === id)?.quantity;
    } else {
      return 0;
    }
  };

  let filteredList = props.products;

  if (props.searchQuery.length > 0) {
    const newList = props.products?.filter((item: any) => {
      const regex = new RegExp(props.searchQuery, "gi");
      return item.name.match(regex) || item.price.toString()?.match(regex);
    });
    filteredList = newList;
  }

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-12">
          {!productLoading && (
            <h3 className="text-center my-3">Products Page</h3>
          )}
        </div>
        {productLoading ? (
          <div className="w-100 h-100">
            <div className="d-flex align-items-center justify-content-center">
              <h2>Loading</h2>
            </div>
          </div>
        ) : (
          <>
            {filteredList?.length > 0 ? (
              <>
                {filteredList?.map((item: any) => (
                  <React.Fragment key={item.id}>
                    <Product
                      item={item}
                      handleClick={handleProductsCart}
                      defaultValue={getCartQuantityNumber(item._id)}
                    />
                  </React.Fragment>
                ))}
              </>
            ) : (
              <div className="w-100 h-100">
                <div className="d-flex align-items-center justify-content-center">
                  <h5>No Products Available</h5>
                </div>
              </div>
            )}
          </>
        )}
      </div>
    </div>
  );
}

const mapStateToProps = (state: RootState) => ({
  products: state.productData.data?.data,
  productLoading: state.productData.isFetching,
  cartItems: state.cartData.items,
  searchQuery: state.searchData.searchQuery,
});

const mapDispatchToProps = {
  getProducts: getProducts,
  ActionAddToCart: AddToCart,
  ActionRemoveFromCart: RemoveFromCart,
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
export default connector(HomePage);
