import React, { ReactElement, useState } from "react";
import { connect, ConnectedProps, useDispatch } from "react-redux";
import { clearCart } from "store/modules/Cart/actions";
import { getProducts } from "store/modules/Product/productReducer";
import { RootState } from "store/root-reducer";
import { CartForm } from "./CartForm";
import CartTable from "./CartTable";

//from boilerplate

interface Props extends PropsFromRedux {}

function CartPage(props: Props): ReactElement {
  const [formData, setFormData] = useState({});

  const handleSubmit = (data: any) => {
    setFormData(data);
    props.clearCart();
  };
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-12">
          <h3 className="text-center my-3">Cart Page</h3>
          <div className="col-12">
            <CartTable cartItems={props.cartItems} products={props.products} />
          </div>
          <div className="col-12">
            <CartForm handleSubmit={handleSubmit} />
          </div>
        </div>
      </div>
      {Object.values(formData).length ? (
        <div className="row mt-5 mx-5">
          <div className="row">
            <div className="col-12 my-2">
              <h3>Order JSON</h3>
            </div>
          </div>
          <div className="col-12">
            <pre className="prettyprint">
              {JSON.stringify(formData, null, 2)}
            </pre>
          </div>
        </div>
      ) : null}
    </div>
  );
}

const mapStateToProps = (state: RootState) => ({
  cartItems: state.cartData.items,
  products: state.productData?.data?.data,
});

const mapDispatchToProps = {
  getProducts: getProducts,
  clearCart: clearCart
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
export default connector(CartPage);
