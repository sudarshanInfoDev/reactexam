import { Formik, Form } from "formik";
import * as React from "react";
import { useSelector } from "react-redux";
import { handleInputChange } from "react-select/src/utils";
import { RootState } from "store/root-reducer";
import * as Yup from "yup";

export interface ICartFormProps {
  handleSubmit: any;
}

export function CartForm(props: ICartFormProps) {
  const { handleSubmit } = props;

  const cartItems = useSelector((state: RootState) => state.cartData.items);

  const itemsSubmitted = cartItems.map((item: any) => {
    const obj = {
      product: item.name,
      price: item.price,
      quantity: item.quantity,
    };
    return obj;
  });

  const totalItemPrice = itemsSubmitted?.reduce(
    (n, { quantity, price }) => n + quantity * price,
    0
  );

  const formFields = [
    {
      type: "text",
      name: "fullname",
      label: "Full Name",
    },
    {
      type: "text",
      name: "address",
      label: "Address",
    },
    {
      type: "text",
      name: "contact",
      label: "Contact Number",
    },
  ];
  return (
    <div>
      <Formik
        initialValues={{
          fullname: "",
          address: "",
          contact: "",
        }}
        validationSchema={Yup.object({
          fullname: Yup.string().required("Name is required"),
          address: Yup.string().required("Address is required"),
          contact: Yup.string().required("Contact is required"),
        })}
        onSubmit={(values, { resetForm }) => {
          handleSubmit({
            ...values,
            order: { items: itemsSubmitted, total: totalItemPrice },
          });
          resetForm();
        }}
      >
        {({ values, handleChange, handleBlur, errors, touched }) => (
          <Form>
            <div className="row">
              {formFields.map((item) => (
                <div key={item.name} className="col-4">
                  {console.log(errors)}
                  <div className="form-group">
                    <label htmlFor="">{item.label}</label>
                    <input
                      type={item.type}
                      className="form-control"
                      name={item.name}
                      value={values[item.name]}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                    {errors[item.name] && touched[item.name] && (
                      <>
                        <small className="text-danger ">
                          {errors[item.name]}
                        </small>
                      </>
                    )}
                  </div>
                </div>
              ))}

              <div className="col-12">
                <button
                  disabled={cartItems.length < 1}
                  className="btn btn-success"
                  type="submit"
                >
                  Submit
                </button>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
}
