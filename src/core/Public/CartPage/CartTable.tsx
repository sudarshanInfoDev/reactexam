import React from "react";
import { useDispatch } from "react-redux";
import { AddToCart, RemoveFromCart } from "store/modules/Cart/actions";

const CartTable = ({ cartItems, products }) => {
  const dispatch = useDispatch();

  const [cartItem, setCartItem] = React.useState(0);

  const handleProductsCart = (productId, type) => {
    const productSelected = products?.find((x: any) => x?._id === productId);
    if (type === "add") {
      dispatch(AddToCart(productSelected));
    } else if (type === "remove") {
      dispatch(RemoveFromCart(productSelected));
    }
  };

  const getCartQuantityNumber = (id) => {
    const isInCart = cartItems.find((x) => x?._id === id);
    if (isInCart) {
      return cartItems.find((x) => x?._id === id)?.quantity;
    } else {
      return 0;
    }
  };

  //   const totalPrice = cartItems((item: any )=> )

  const totalItemPrice = cartItems.reduce(
    (n, { quantity, price }) => n + quantity * price,
    0
  );

  return (
    <>
      <table className="table table-striped table-bordered">
        <tbody>
          <tr>
            <th scope="row">
              <span></span>#
            </th>
            <th scope="row">
              <span></span>Product Name
            </th>
            <th scope="row" className="text-center">
              <span></span>Price
            </th>
            <th scope="row" className="text-center">
              <span></span>Quantity
            </th>
            <th scope="row" className="text-center">
              <span></span>Total
            </th>
          </tr>
          {cartItems?.length > 0 ? (
            cartItems.map((product, index) => (
              <tr key={product.id}>
                <th scope="row">
                  <span></span>
                  {index + 1}
                </th>
                <td className="w-custom">{product.name}</td>
                <td className="text-center">
                  Rs. {product.discountPrice || product.price}
                </td>
                <td className="text-center">
                  <div className="add__to__cart__button">
                    <button
                      className="btn btn-sm btn-danger"
                      onClick={() => {
                        setCartItem(cartItem - 1);
                        handleProductsCart(product._id, "remove");
                      }}
                    >
                      -
                    </button>
                    <span style={{ width: 100 }}>
                      {getCartQuantityNumber(product._id)}
                    </span>
                    <button
                      className="btn btn-sm btn-success"
                      onClick={() => {
                        setCartItem(cartItem + 1);
                        handleProductsCart(product._id, "add");
                      }}
                    >
                      +
                    </button>
                  </div>
                </td>
                <td className="text-center">
                  Rs. {Number(product.quantity * product.price).toFixed(2)}
                </td>
              </tr>
            ))
          ) : (
            <tr>
              <td colSpan={5} className="text-center font-weight-bold">
                No Datas Available !!!. Please add items to cart first.
              </td>
            </tr>
          )}
          <tr className="text-center">
            <td colSpan={4}></td>
            <td>{totalItemPrice && Number(totalItemPrice).toFixed(2)}</td>
          </tr>
        </tbody>
      </table>
    </>
  );
};

export default CartTable;
