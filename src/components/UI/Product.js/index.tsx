import * as React from "react";

export interface IProductProps {
  item: any;
  handleClick: any;
  defaultValue: any;
}

export default function Product(props: IProductProps) {
  const { defaultValue, handleClick, item } = props;

  const [cartItem, setCartItem] = React.useState(0);

  React.useEffect(() => {
    if (defaultValue) {
      setCartItem(defaultValue);
    }
  }, [defaultValue]);


  return (
    <>
      <div className="card my-2 mx-2" style={{ width: "18rem" }}>
        {/* <img className="card-img-top" src="...  " alt="Card image cap" /> */}
        <div className="card-body">
          <h5 className="card-title">{item.name}</h5>
          <div dangerouslySetInnerHTML={{ __html: item.description }}></div>
          <p>Rs. {item.price}</p>
          {}
          {cartItem === 0 ? (
            <button
              className="btn btn-sm btn-primary"
              onClick={() => {
                setCartItem(cartItem + 1);
                handleClick(item._id, "add");
              }}
            >
              Add To Cart
            </button>
          ) : (
            <div className="add__to__cart__button">
              <button
                className="btn btn-sm btn-danger"
                onClick={() => {
                  setCartItem(cartItem - 1);
                  handleClick(item._id, "remove");
                }}
              >
                -
              </button>
              <span style={{width: 100}}>{cartItem}</span>
              <button
                className="btn btn-sm btn-success"
                onClick={() => {
                  setCartItem(cartItem + 1);
                  handleClick(item._id, "add");
                }}
              >
                +
              </button>
            </div>
          )}
        </div>
      </div>
    </>
  );
}
