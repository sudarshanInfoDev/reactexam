import { Form, Formik } from "formik";
import * as React from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useHistory } from "react-router-dom";
import { changeSearchQuery } from "store/modules/Search/action";
import { RootState } from "store/root-reducer";

interface Props {}

const Navbar = (props: Props) => {
  const dispatch = useDispatch();
  const histroy = useHistory();
  const [itemCount, setItemCount] = React.useState(0);

  const cartItems = useSelector((state: RootState) => state.cartData.items);

  React.useEffect(() => {
    const totalItems = cartItems.reduce((sum, acc) => sum + acc.quantity, 0);
    if (totalItems) {
      setItemCount(totalItems);
    } else {
      setItemCount(0);
    }
  }, [cartItems]);

  const handlePushTOCart = () => {
    histroy.push("/cart");
  };

  const handleSearch = (e) => {
    dispatch(changeSearchQuery(e.target.value));
  };

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <a className="navbar-brand" href="#">
          Navbar
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item ">
              <NavLink to="/" className="nav-link">
                Home <span className="sr-only">(current)</span>
              </NavLink>
            </li>
          </ul>
          <div style={{ width: 300 }}>
            <input
              className="form-control mr-sm-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
              name="search"
              onChange={handleSearch}
            />
          </div>

          <button
            disabled={cartItems.length < 1}
            onClick={handlePushTOCart}
            className="btn btn-primary mx-2"
          >
            Cart [ {itemCount} ]
          </button>
        </div>
      </nav>
    </>
  );
};

export default Navbar;
