import { Dispatch } from "redux";
import { AppThunk } from "../..";
import { apiList } from "../../actionNames";
import initDefaultAction from "../../helper/default-action";
import initDispatchTypes from "../../helper/default-action-type";
import initDefaultReducer from "../../helper/default-reducer";
import initialState from "../../helper/default-state";


type ProductResponse = {
    /**jwt access token */
    "status": string,
    "results": number,
    "data": any,
}

const initialProductState = initialState;
const apiDetails = Object.freeze(apiList.public.getProduct);

export default function productReducer(state = initialProductState, action: DefaultAction): DefaultState<ProductResponse> {
    const stateCopy = Object.assign({}, state);
    const actionName = apiDetails.actionName;

    return initDefaultReducer(actionName, action, stateCopy);
}

export const getProducts = (): AppThunk<Promise<ProductResponse>> => async (dispatch: Dispatch) => {
    const productData = await initDefaultAction(apiDetails, dispatch, { disableSuccessToast: true });

    if (productData && productData?.data) {
        const dispatchTypes = initDispatchTypes(apiDetails.actionName);
        dispatch({ type: dispatchTypes.successDispatch, payload: { status: 1, data: productData.data  } });

    }

    return productData;
};