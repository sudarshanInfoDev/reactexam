import { Dispatch } from "redux";

export const changeSearchQuery = (data) => (dispatch:Dispatch) => {
    dispatch({
        type: "SEARCH_CHANGE",
        payload: data
    })
}