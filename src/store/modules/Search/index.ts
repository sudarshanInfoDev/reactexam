
interface IAction {
    type: string,
    payload?: any
}

interface ISearch {
    searchQuery: string,

}
const inititalState: ISearch = {
    searchQuery: "",
};

const searchReducer = (state = inititalState, action: IAction) => {
    const { type, payload } = action;

    switch (type) {
  
        case "SEARCH_CHANGE":
            return {
                ...state,
                searchQuery: payload
            }
        default:
            return state
    }
};

export default searchReducer