import { ADD_TO_CART, CLEAR_CART, REMOVER_FROM_CART } from "./actionTypes"
import { Dispatch } from "redux";

export const AddToCart = (data: any) => (dispatch: Dispatch) => {
    dispatch({
        type: ADD_TO_CART,
        payload: data
    })
}
export const RemoveFromCart = (data: any) => (dispatch: Dispatch) => {
    dispatch({
        type: REMOVER_FROM_CART,
        payload: data
    })
}
export const clearCart = () => (dispatch: Dispatch) => {
    dispatch({
        type: CLEAR_CART
    })
}