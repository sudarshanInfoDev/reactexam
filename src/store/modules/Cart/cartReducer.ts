import { ADD_TO_CART, CLEAR_CART, REMOVER_FROM_CART } from "./actionTypes";

interface IAction {
    type: string,
    payload?: any
}

interface IProductItem {
    _id: any,
    name: string,
    price: string,
    quantity: any

}
interface IItemsArray {
    items: IProductItem[]
}


const inititalState: IItemsArray = {
    items: [],
};

const cartReducer = (state = inititalState, action: IAction) => {
    const { type, payload } = action;

    switch (type) {
        case ADD_TO_CART:
            const copyAdd = [...state.items]
            const isInCartAdd = copyAdd.find((x: any) => x?._id === payload?._id) ? true : false
            let newAdd: IProductItem[] = []

            if (isInCartAdd) {
                const index = copyAdd.findIndex((x: any) => x?._id === payload?._id)
                const quantity = copyAdd.find((x: any) => x?._id === payload?._id)?.quantity
                copyAdd[index].quantity = quantity + 1
                newAdd = copyAdd

            } else {
                newAdd = [...copyAdd, { ...payload, quantity: 1 }]
            }
            return {
                ...state,
                items: newAdd
            }
        case REMOVER_FROM_CART:
            const copyRemove = [...state.items]
            const isInCartRemove = copyRemove.find(x => x?._id === payload?._id) ? true : false

            let newRemove: IProductItem[] = []

            if (isInCartRemove) {
                const index = copyRemove.findIndex((x: any) => x?._id === payload?._id)
                const quantity = copyRemove.find((x: any) => x?._id === payload?._id)?.quantity

                if (quantity > 1) {
                    copyRemove[index].quantity = quantity - 1
                    newRemove = copyRemove
                } else {
                    newRemove = copyRemove?.filter((x: any) => x?._id !== payload._id)
                }
            } else {
                newRemove = [...state.items]
            }
            return {
                ...state,
                items: newRemove
            }
        case CLEAR_CART:
            return {
                ...state,
                items: []
            }
        default:
            return state
    }
};

export default cartReducer